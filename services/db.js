var mongoose = require('mongoose');

var address = process.env.MONGODB_PORT_27017_TCP_ADDR; 
var port = process.env.MONGODB_PORT_27017_TCP_PORT; 

mongoose.connect("mongodb://" + address + ":" + port + "/hello-world"); 