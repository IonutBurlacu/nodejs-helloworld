var response = {
	success: function(res, message, data){
		res.json({
			code: 2000,
			message: message,
			data: data
		})
	},
	error: function(res, code, message, data){
		res.json({
			code: code,
			message: message,
			data: data
		})
	}
}

module.exports = response;