var Book = require('../../models/book');
var response = require('../../services/response');

var bookCtrl = {
	getBooks: function(req, res, next){
		/* Check if lang parameter is provided and is valid */
		var lang = req.query.lang;
		if(typeof lang === "undefined"){
			response.error(res, 1000, "Lang parameter is required!", {});
		}
		else if(lang != "en" && lang != "he"){
			response.error(res, 1001, "Lang parameter must be equal to 'en' or 'he'!", {});
		}
		else {
			/* Get books from DB */
			Book.find({}, function(err, data){
				if(err) res.send(err);

				var books = [];
				data.forEach(function(book){
					books.push({
						title: book.title[lang],
						author: book.author[lang],
						description: book.description[lang]
					});
				});

				/* Send back to user as json */
				response.success(res, "", {books: books});
			});
		}
	}
};

module.exports = bookCtrl;