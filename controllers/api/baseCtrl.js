var config = require('../../config');
var response = require('../../services/response');

var baseCtrl = {
	getVersion: function(req, res, next){
		response.success(res, "", {version: config.version});
	}
};

module.exports = baseCtrl;