var mongoose = require('mongoose');
var db = require('../services/db');

var BookSchema  = new mongoose.Schema({
		title: {
			en: String,
			he: String
		},
		author: {
			en: String,
			he: String
		},
		description: {
			en: String,
			he: String
		}
	}, {
		collection: 'book'
	});

var Book = mongoose.model("Book", BookSchema);

module.exports = Book;