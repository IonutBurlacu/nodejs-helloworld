var express = require('express')
var bodyParser = require('body-parser');
var fs = require('fs');
var FileStreamRotator = require('file-stream-rotator')
var morgan = require('morgan');
var app = express();
var server = require('http').createServer(app);

var logDirectory = __dirname + '/logs';
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

var accessLogStream = FileStreamRotator.getStream({
  filename: logDirectory + '/access-%DATE%.log',
  frequency: 'daily',
  verbose: false,
  date_format: "YYYY-MM-DD"
});

app.use(morgan('[:date[web]] From: :remote-addr, Response-time :response-time ms, Method: ":method :url HTTP/:http-version", Status: :status, Content-length: :res[content-length], Referrer: ":referrer", User-agent: ":user-agent"', {stream: accessLogStream}));

/* App routes */

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

var apiRoutes = require('./routes/api/index');
app.use('/api', apiRoutes);

/* Start the server */

var port = 27000;
server.listen(port);
console.log('Listening on port ' + port);
