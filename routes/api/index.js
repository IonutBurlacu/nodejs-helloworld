var router = require('express').Router();
var config = require('../../config');
var bookCtrl = require('../../controllers/api/bookCtrl');
var baseCtrl = require('../../controllers/api/baseCtrl');

/* GET REST Api version */
router.get('/version', baseCtrl.getVersion);

/* GET Books list. */
router.get('/' + config.version + '/books', bookCtrl.getBooks);

module.exports = router;
