var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var mongoose = require('mongoose');
 
describe('Routing', function() {
    var url = 'http://localhost:27000';
    var version = "";
  
    /* Test for getVersion service */
    describe('getVersion', function(){
        it('should return the version of api in data object with version key', function(done){
            request(url)
            .get('/api/version')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res){
                if(err) {
                    throw err;
                }

                res.body.code.should.equal(2000);
                res.body.message.should.equal("");
                res.body.data.should.be.type('object');
                res.body.data.should.have.property('version');
                res.body.data.version.should.be.type('string');

                version = res.body.data.version;

                done();
            });
        });
    });

    /* Test for getBooks service */
    describe('getBooks', function() {
        it('should return error because lang is not present', function(done) {
            request(url)
	        .get('/api/' + version + '/books')
            .expect('Content-Type', /json/)
            .expect(200)
    	    .end(function(err, res) {
                if (err) {
                    throw err;
                }
        
                res.body.code.should.equal(1000);
                res.body.message.should.equal('Lang parameter is required!');
                done();
            });
        });
        it('should return error because lang is not equal to `en` or `he`', function(done){
            request(url)
            .get('/api/' + version + '/books?lang=ro')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res){
                if(err){
                    throw err;
                }

                res.body.code.should.equal(1001);
                res.body.message.should.equal("Lang parameter must be equal to 'en' or 'he'!");
                done();
            });
        });
        it('should return in data key and object with key `books` containing an array', function(done){
            request(url)
            .get('/api/' + version + '/books?lang=he')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res){
                if(err){
                    throw err;
                }

                res.body.code.should.equal(2000);
                res.body.message.should.equal("");
                res.body.data.should.be.type('object');
                res.body.data.should.have.property('books');
                res.body.data.books.should.be.an.instanceOf(Array);
                done();
            });
        });
    });
});