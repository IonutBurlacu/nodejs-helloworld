These are the files needed to build a nodejs docker image.

Instructions:

### Cd to the directory with nodejs files (controllers, routes, Dockerfile, etc.)
1. cd /path/to/nodejs/dir

### Remove the old image in case you have one already
2. docker rmi -f node-image:0.1.0

### Build a new image
3. docker build -t node-image:0.1.0 .

### Remove the old container if you have one already. 
4. docker rm -f node-container

### Run a new container. It will start in interactive mode, this means you will have access to the container's bash once you run the following command. The `pwd` variable means the current directory in ubuntu, it might differ in other OS. We link the mongo-container with the node-container so that they can comunicate throught docker0 protocol. The server will be accessible through: http://localhost:27000/ .
5. docker run -i -t -p 27000:27000 --name node-container -v `pwd`:/src --link mongo-container:mongodb node-image:0.1.0

### To start the nodejs server type the following commands in the bash that has opened after running `run` command. It will install all dependencies and will start the server. Nodemon is used to watch for changes in the folder. For example, if we modify a file from this folder, the server will automaticly restart to see the changes.
6. npm install && npm install -g nodemon mocha && nodemon index.js

### Once you type exit, the container will shutdown.
7. exit
